import os
import re
import time
import shutil
import requests
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.service import Service
from selenium.common.exceptions import (
    TimeoutException,
    NoSuchElementException,
    StaleElementReferenceException,
)
from webdriver_manager.firefox import GeckoDriverManager


def delete_element(driver, element):
    """Delete element from the DOM using JavasScript"""
    deleted = driver.execute_script("return arguments[0].remove();", element)
    return deleted, element


def delete_cookies(driver, timeout):
    """Delete TrustArc cookie modal from the DOM"""
    css_classes = ["truste_overlay", "truste_box_overlay"]
    try:
        for css_class in css_classes:
            element = wait(driver, timeout).until(
                EC.visibility_of_element_located((By.CLASS_NAME, css_class))
            )
            delete_element(driver, element)
    except (NoSuchElementException, TimeoutException):
        return False
    return True


class Crawler:
    def __init__(self, query):
        self.timeout = 25
        self.query = query
        self.query_url = "https://www.flickr.com/search/?text={}&view_all=1&media=photos&dimension_search_mode=min&height=640&width=640".format(
            self.query
        )
        self.driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
        self.driver.maximize_window()
        self.driver.get(self.query_url)

        delete_cookies(self.driver, self.timeout)

    def crawl(self):
        urls = []

        def check_if_exists(by=By.CLASS_NAME, value="infinite-scroll-load-more"):
            try:
                self.driver.find_element(by=by, value=value)
            except NoSuchElementException:
                return False
            return True

        last_height = self.driver.execute_script("return document.body.scrollHeight")
        while True:
            self.driver.execute_script(
                "window.scrollTo(0, document.body.scrollHeight);"
            )
            # TODO: Implement a different way of waiting for the page to scroll down
            # Maybe check if footer is in view?
            time.sleep(2)
            new_height = self.driver.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                load_more = check_if_exists()
                if load_more:
                    # retrieve all the current items
                    items = wait(self.driver, self.timeout).until(
                        EC.presence_of_all_elements_located(
                            (By.CLASS_NAME, "photo-list-photo-interaction")
                        )
                    )

                    for item in items:
                        try:
                            url = item.find_element(
                                by=By.TAG_NAME, value="a"
                            ).get_attribute("href")
                        except StaleElementReferenceException:
                            continue
                        urls.append(url)
                        delete_element(self.driver, item)

                    # click 'load more button'
                    wait(self.driver, self.timeout).until(
                        EC.element_to_be_clickable(
                            (By.CLASS_NAME, "infinite-scroll-load-more")
                        )
                    ).find_element(by=By.TAG_NAME, value="button").click()
                else:
                    self.driver.get(self.query_url)
                    print("new url")
                    time.sleep(20)
                    break
            last_height = new_height
        # remove duplicates
        urls = list(dict.fromkeys(urls))
        # write to text file
        with open("./output/txt/{}.txt".format(self.query), "w") as f:
            for url in urls:
                f.write(url + "\n")
        self.driver.quit()
        return urls


class Scraper:
    def __init__(self, urls):
        self.urls = urls
        self.folder = re.split("\/|, |\.", self.urls.name)[-2]
        self.driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
        self.driver.maximize_window()

        try:
            os.makedirs("./output/images/{}".format(self.folder))
        except:
            pass

    def download_images(self):
        for url in self.urls:
            for retry in range(3):
                try:
                    self.driver.get(url)
                    deleted = delete_cookies(self.driver, 25)
                    if deleted:
                        image_url = self.driver.find_element(
                            By.CLASS_NAME, "main-photo"
                        ).get_attribute("src")

                        filename = image_url.split("/")[-1]

                        r = requests.get(image_url, stream=True)
                        if r.status_code == 200:
                            r.raw.decode_content = True

                            with open(
                                "./output/images/{}/{}".format(self.folder, filename),
                                "wb",
                            ) as f:
                                shutil.copyfileobj(r.raw, f)

                            print("{} successfully downloaded".format(filename))
                        else:
                            print("Error downloading file {}".format(filename))
                except NoSuchElementException:
                    print("RETRY [{}/3]: Cannot find element".format(retry))
                    pass
                else:
                    break  # Next image

        self.driver.quit()
