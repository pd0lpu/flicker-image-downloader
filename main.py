#!/usr/bin/env python3
import argparse
from helpers.flickr import Crawler, Scraper


def parse_arguments():
    """Read arguments from the command line."""
    parser = argparse.ArgumentParser(description="Arguments get parsed by --commands")
    subparser = parser.add_subparsers(dest="mode", required=True)
    crawler = subparser.add_parser("crawler")
    scraper = subparser.add_parser("scraper")

    # Crawler arguments
    crawler.add_argument(
        "-q",
        type=str,
        metavar="QUERY",
        required=True,
        default="robot",
        help="Search query used on Flickr",
    )

    # Scraper arguments
    scraper.add_argument(
        "-t",
        type=argparse.FileType("r"),
        metavar="TXT",
        required=True,
        help="Textfile (txt) containing Flickr images",
    )

    args = parser.parse_args()

    return args


def main():
    if args.mode == "crawler":
        Crawler(args.q).crawl()

    elif args.mode == "scraper":
        Scraper(args.t).download_images()


if __name__ == "__main__":
    args = parse_arguments()
    main()
